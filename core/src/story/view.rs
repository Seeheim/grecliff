use crate::engine::WorldState;
use crate::story::content::Content;
use crate::story::ContentItem;
use crate::story::{Choice, Scene};

pub struct SceneView<'a> {
    scene: &'a Scene,
    content: &'a Content,
}

impl<'a> SceneView<'a> {
    pub fn new(scene: &'a Scene, content: &'a Content) -> Self {
        SceneView { scene, content }
    }

    pub fn id(&'a self) -> &'a str {
        &self.scene.id
    }
    pub fn title(&'a self) -> Option<String> {
        self.content.get_message(&self.scene.title_id())
    }

    pub fn description(&'a self, world_state: &WorldState) -> Vec<String> {
        let mut message: Vec<String> = Default::default();
        for section in self.scene.sections(world_state) {
            if let Some(section_content) = self.content.get_message(&section.description_id()) {
                message.push(section_content);
            }
        }
        message
    }

    pub fn choice(&self, choice_id: &str) -> Option<ChoiceView> {
        self.scene
            .choice(choice_id)
            .map(|c| ChoiceView::new(c, self.content))
    }

    pub fn choices(&self, world_state: &WorldState) -> Vec<ChoiceView<'a>> {
        self.scene
            .choices(world_state)
            .into_iter()
            .map(|c| ChoiceView::new(&c, self.content))
            .collect()
    }

    pub fn next_scene(&self) -> Option<&String> {
        self.scene.next_scene.as_ref()
    }
}

pub struct ChoiceView<'a> {
    choice: &'a Choice,
    content: &'a Content,
}

impl<'a> ChoiceView<'a> {
    fn new(choice: &'a Choice, content: &'a Content) -> Self {
        ChoiceView { choice, content }
    }
    pub fn id(&self) -> &str {
        self.choice.id()
    }

    pub fn title(&self) -> String {
        self.content
            .get_message(&self.choice.title_id())
            .unwrap_or_else(|| panic!("No item for {}", self.choice.title_id()))
    }

    pub fn description(&self) -> String {
        self.content
            .get_message(&self.choice.description_id())
            .unwrap()
    }

    pub fn execute(&self, world_state: &mut WorldState) {
        self.choice.execute(world_state);
    }

    pub fn next_scene(&self) -> Option<&String> {
        self.choice.next_scene.as_ref()
    }
}
