use fluent::{FluentArgs, FluentBundle, FluentResource};
use serde::export::fmt::Debug;
use serde::export::Formatter;
use std::collections::HashMap;
use std::fmt;
use std::rc::Rc;
use unic_langid::LanguageIdentifier;

#[derive(Clone, Default)]
pub struct Content {
    current_lang: LanguageIdentifier,
    bundles: HashMap<LanguageIdentifier, Rc<FluentBundle<FluentResource>>>,
}

impl Debug for Content {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        f.write_fmt(format_args!("Content"))?;
        Ok(())
    }
}

impl Content {
    pub fn add_bundle(
        &mut self,
        lang_id: LanguageIdentifier,
        bundle: FluentBundle<FluentResource>,
    ) {
        self.bundles.insert(lang_id, Rc::new(bundle));
    }

    pub fn set_language(&mut self, lang_id: LanguageIdentifier) {
        self.current_lang = lang_id
    }

    pub fn get_message(&self, msg: &str) -> Option<String> {
        let msg = msg.to_lowercase();
        let bundle = self.bundles.get(&self.current_lang.clone())?;
        let msg = bundle.get_message(&msg)?;

        let args = FluentArgs::new();
        let mut errors = vec![];
        let pattern = msg.value.expect("Message has no value.");
        Some(
            bundle
                .format_pattern(&pattern, Some(&args), &mut errors)
                .to_string(),
        )
    }
}
