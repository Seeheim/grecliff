use crate::story::io::ProjectError::{InvalidStoryStructure, MissingLanguageItem};
use crate::story::{Story, Structure};
use fluent::{FluentBundle, FluentResource};
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::io::Write;

use unic_langid::langid;
use unic_langid::LanguageIdentifier;

fn make_bundle(
    lang_id: LanguageIdentifier,
    resource_str: String,
) -> Result<FluentBundle<FluentResource>, Box<dyn Error>> {
    let resource = FluentResource::try_new(resource_str).unwrap();
    let mut bundle: FluentBundle<FluentResource> = FluentBundle::new(&[lang_id]);
    bundle.add_resource(resource).unwrap();
    Ok(bundle)
}

#[derive(Debug)]
pub enum ProjectError {
    InvalidStoryStructure { err: Box<dyn Error> },
    MissingLanguageItem { key: String },
    DuplicateLanguageItem { key: String },
}

pub struct StoryBuilder {
    story: Story,
}

impl StoryBuilder {
    pub fn new(story_json: &str) -> Result<Self, Box<dyn Error>> {
        let structure: Structure = serde_json::from_str(story_json).unwrap();
        let mut story = Story::default();
        story.structure = structure;
        Ok(StoryBuilder { story })
    }

    pub fn add_bundle(
        &mut self,
        lang_id: String,
        resource_str: String,
    ) -> Result<(), Box<dyn Error>> {
        let lang_id: LanguageIdentifier = lang_id.parse()?;
        let bundle = make_bundle(lang_id.clone(), resource_str).unwrap();
        self.story.content.add_bundle(lang_id, bundle);
        Ok(())
    }

    pub fn verify_language_completeness(
        &mut self,
        lang: LanguageIdentifier,
    ) -> Result<(), ProjectError> {
        self.story.set_language(lang);
        for content_item in self.story.iter_content_ids() {
            let language_item = self.story.content.get_message(&content_item);
            if language_item.is_none() {
                return Err(MissingLanguageItem { key: content_item });
            };
        }
        Ok(())
    }

    pub fn finish(mut self) -> Result<Story, ProjectError> {
        self.verify_language_completeness(langid!("en-US"))?;
        Ok(self.story)
    }
}

pub fn build_story(story_string: &str, langs: &[(&str, &str)]) -> Result<Story, ProjectError> {
    let mut builder = match StoryBuilder::new(story_string) {
        Ok(builder) => builder,
        Err(err) => return Err(InvalidStoryStructure { err }),
    };

    for (lang, content) in langs {
        builder
            .add_bundle((*lang).to_string(), (*content).to_string())
            .unwrap_or_else(|_| panic!("Invalid fluent bundle for {}", lang))
    }
    builder.finish()
}

pub fn read_structure(path: &str) -> Result<Structure, Box<dyn Error>> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(serde_json::from_str(&contents)?)
}

pub fn create_fluent_template(structure: &Structure, path: &str) -> Result<(), std::io::Error> {
    let items = structure.iter_content_ids();

    let mut file = File::create(path)?;
    for item in &items {
        file.write_all(format!("{} = \n", item).as_bytes())?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    static STORY_SKELETON: &str = include_str!("../../../assets/story.json");
    static CONTENT_BUNDLE: &str = include_str!("../../../assets/content/en-US.txt");

    #[test]
    fn test_story_loading() {
        let langs = &[("en-US", CONTENT_BUNDLE)];
        build_story(STORY_SKELETON, langs).unwrap();
    }
}
