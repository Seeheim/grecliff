use crate::story::{ChoiceView, SceneView, Story};
use std::collections::HashMap;

pub struct Engine {
    stage: Story,
    pub state: WorldState,
    current_scene_id: String,
}

impl From<Story> for Engine {
    fn from(story: Story) -> Self {
        let start_scene_id = story.get_start_scene().unwrap().id().to_string();
        Engine {
            stage: story,
            state: WorldState::default(),
            current_scene_id: start_scene_id,
        }
    }
}

impl Engine {
    pub fn story(&self) -> &Story {
        &self.stage
    }

    pub fn current_scene(&self) -> SceneView {
        self.stage.get_scene_view(&self.current_scene_id).unwrap()
    }

    /// Returns the current scene description as a list of paragraphs.
    pub fn current_scene_description(&self) -> Vec<String> {
        self.current_scene().description(&self.state)
    }

    pub fn current_choices(&self) -> Vec<ChoiceView> {
        self.current_scene().choices(&self.state)
    }

    pub fn select_choice(&mut self, choice_id: &str) {
        let current_scene = self.stage.get_scene_view(&self.current_scene_id).unwrap();
        let selected_choice = &current_scene.choice(choice_id).unwrap();
        let next_scene_id = self.next_scene_id(choice_id);
        if let Some(next_scene_id) = next_scene_id {
            self.current_scene_id = next_scene_id
        }
        selected_choice.execute(&mut self.state);
    }

    fn next_scene_id(&self, choice_id: &str) -> Option<String> {
        let current_scene = self.current_scene();
        let selected_choice = current_scene.choice(choice_id)?;
        let next_scene_id = selected_choice.next_scene();
        if let Some(next_scene_id) = next_scene_id {
            return Some(next_scene_id.to_string());
        }
        let next_scene_id = current_scene.next_scene();
        if let Some(next_scene_id) = next_scene_id {
            return Some(next_scene_id.to_string());
        }
        None
    }
}

#[derive(Debug, Default)]
pub struct WorldState {
    pub counters: HashMap<String, i32>,
}
