use serde_derive::{Deserialize, Serialize};

use crate::engine::WorldState;
use crate::story::content::Content;
pub use crate::story::view::*;
use unic_langid::LanguageIdentifier;

mod content;
pub mod io;
pub mod view;

static START_ID: &str = "start";
static TITLE_ID: &str = "title";

fn make_title_id(id: &str) -> String {
    format!("{}-title", id)
}

fn make_description_id(id: &str) -> String {
    format!("{}-description", id)
}

trait ContentItem {
    fn id(&self) -> &str;
    fn title_id(&self) -> String {
        make_title_id(self.id())
    }
    fn description_id(&self) -> String {
        make_description_id(self.id())
    }
}

trait Validatable {
    fn preconditions(&self) -> Vec<Condition>;
    fn is_valid(&self, world_state: &WorldState) -> bool {
        self.preconditions()
            .iter()
            .all(|condition| condition.is_valid(world_state))
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Structure {
    scenes: Vec<Scene>,
}

impl Structure {
    pub(crate) fn iter_content_ids(&self) -> Vec<String> {
        self.scenes
            .iter()
            .flat_map(|scene| scene.content_ids())
            .collect()
    }
}

#[derive(Clone, Debug, Default)]
pub struct Story {
    version: Option<String>,
    structure: Structure,
    content: Content,
}

impl Story {
    pub fn title(&self) -> String {
        self.content
            .get_message(TITLE_ID)
            .unwrap_or_else(|| "Untitled".to_string())
    }

    pub fn get_start_scene(&self) -> Option<SceneView> {
        self.get_scene_view(START_ID)
    }

    pub fn get_scene_view(&self, scene_id: &str) -> Option<SceneView> {
        self.structure
            .scenes
            .iter()
            .find(|scene| scene.id == scene_id)
            .map(|scene| SceneView::new(scene, &self.content))
    }

    pub fn set_language(&mut self, lang_id: LanguageIdentifier) {
        self.content.set_language(lang_id);
    }

    pub(crate) fn iter_content_ids(&self) -> Vec<String> {
        self.structure.iter_content_ids()
    }

    pub fn set_version(&mut self, version:String) {
        self.version = Some(version);
    }

    pub fn version(&self) -> Option<String> {
        self.version.clone()
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Event {
    CounterChange { counter: String, delta: i32 },
}

impl Event {
    pub fn execute(&self, world_state: &mut WorldState) {
        match self {
            Event::CounterChange { counter, delta } => {
                let counter = world_state.counters.entry(counter.clone()).or_insert(0);
                *counter += *delta;
            }
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Restriction {
    Greater(i32),
    Less(i32),
    InRange(i32, i32),
}

impl Restriction {
    pub fn evaluate(&self, value: i32) -> bool {
        match self {
            Restriction::Greater(limit) => value > *limit,
            Restriction::Less(limit) => value < *limit,
            Restriction::InRange(lower, upper) => *lower < value && value <= *upper,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Condition {
    pub value: String,
    pub restriction: Restriction,
}

impl Condition {
    pub fn is_valid(&self, world_state: &WorldState) -> bool {
        let value = world_state
            .counters
            .get(&self.value)
            .copied()
            .unwrap_or_else(|| 0);
        self.restriction.evaluate(value)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Choice {
    pub id: String,
    pub events: Vec<Event>,
    pub next_scene: Option<String>,
    pub preconditions: Vec<Condition>,
}

impl Validatable for Choice {
    fn preconditions(&self) -> Vec<Condition> {
        self.preconditions.clone()
    }
}

impl ContentItem for Choice {
    fn id(&self) -> &str {
        &self.id
    }
}

impl Choice {
    pub fn execute(&self, world_state: &mut WorldState) {
        for event in &self.events {
            event.execute(world_state);
        }
    }

    pub fn next_scene(&self) -> Option<&String> {
        self.next_scene.as_ref()
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Scene {
    id: String,
    description: Vec<Section>,
    next_scene: Option<String>,
    choices: Vec<Choice>,
}

impl ContentItem for Scene {
    fn id(&self) -> &str {
        &self.id
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Section {
    id: String,
    preconditions: Vec<Condition>,
}

impl ContentItem for Section {
    fn id(&self) -> &str {
        &self.id
    }
}

impl Section {
    fn new(id: String) -> Self {
        Section {
            id,
            preconditions: Vec::new(),
        }
    }
}

impl Validatable for Section {
    fn preconditions(&self) -> Vec<Condition> {
        self.preconditions.clone()
    }
}

impl From<String> for Section {
    fn from(s: String) -> Self {
        Section::new(s)
    }
}

impl Scene {
    pub fn title_id(&self) -> String {
        make_title_id(&self.id)
    }
    pub fn sections(&self, world_state: &WorldState) -> Vec<Section> {
        self.description
            .iter()
            .filter(|section| section.is_valid(&world_state))
            .cloned()
            .collect()
    }

    fn choices(&self, world_state: &WorldState) -> Vec<&Choice> {
        self.choices
            .iter()
            .filter(|choice| choice.is_valid(&world_state))
            .collect()
    }

    pub fn choice(&self, choice_id: &str) -> Option<&Choice> {
        self.choices.iter().find(|choice| choice.id == choice_id)
    }

    pub(crate) fn content_ids(&self) -> Vec<String> {
        // fixme this is inefficient
        let description_ids = self
            .description
            .iter()
            .flat_map(|section| vec![section.description_id()]);
        let choice_ids = self
            .choices
            .iter()
            .flat_map(|choice| vec![choice.description_id(), choice.title_id()]);

        let mut all_ids: Vec<String> = vec![self.title_id()];
        all_ids.extend(description_ids);
        all_ids.extend(choice_ids);

        all_ids
    }

    pub fn next_scene(&self) -> Option<&String> {
        self.next_scene.as_ref()
    }
}
