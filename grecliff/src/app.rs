use core::engine::Engine;
use core::story::{ChoiceView, Story};
use yew::prelude::*;


#[derive(Clone)]
pub enum Message {
    Click,
    Choice { id: String },
}

#[derive(Clone, Default, Debug)]
struct State {
    debug: bool,
}

#[derive(Clone, Debug, Properties)]
pub struct StoryAppProperties {
    pub story: Story,
}

pub struct StoryApp {
    state: State,
    engine: Engine,
    link: ComponentLink<Self>,
}

impl Component for StoryApp {
    type Message = Message;
    type Properties = StoryAppProperties;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        StoryApp {
            engine: props.story.into(),
            link,
            state: Default::default(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Message::Click => unimplemented!(),
            Message::Choice { id } => self.engine.select_choice(&id),
        }
        true
    }

    fn view(&self) -> Html {
        let title = self.engine.story().title();
        let scene = self.engine.current_scene();
        let sections = self.engine.current_scene_description();
        let choices = self.engine.current_choices();
        let choices_html = choices
            .iter()
            .map(|choice| self.render_choice(choice))
            .collect::<Html>();
        html! {
            <>
                <section class="hero is-dark">
                  <div class="hero-body">
                    <div class="container">
                      <p class="title is-capitalized">
                        { title.clone() }
                      </p>
                      <p class="subtitle">
                         { scene.title().as_ref().cloned().unwrap_or("".to_string())  }
                      </p>
                    </div>
                  </div>
                </section>
                <div class="section">
                    <div class="container">
                        <div class="is-family-secondary is-size-5">
                            <div>
                                { for sections.iter().map(|s| self.render_section(s)) }
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section">
                 <div class="container">
                    <div class="columns is-multiline">
                          { choices_html }
                    </div>
                </div>
                </div>
                <div class="section">
                    <div class="container">
                        <div>
                           { self.render_debug_output() }
                        </div>
                    </div>
                 </div>
                 { self.render_footer() }
            </>
        }
    }
}

impl StoryApp {

    fn render_footer(&self) -> Html {
        let version = self.engine.story().version().unwrap_or_else(|| "Unknown".to_string());
        html! {
            <footer class="footer">
                <div class="content has-text-centered">
                    <p>
                      { "Copyright 2020 by Seeheim " }
                    </p>
                     <p>
                      { "Support us on " } <a href="https://patreon.com/seeheim"> { "Patreon" }</a>
                    </p>
                    <p>
                       { "Version " } { version }
                    </p>

                </div>
            </footer>
        }
    }

    fn render_choice(&self, choice: &ChoiceView) -> Html {
        let message = Message::Choice {
            id: choice.id().to_string(),
        };
        html! {
            <div class="column is-6">
            <a href="#" onclick=self.link.callback(move |_| message.clone())>
            <div class="card">
            <header class="card-header">
                <p class="card-header-title is-capitalized">
                   { choice.title().clone() }
                </p>
            </header>
            <div class="card-content">
                <div class="content is-family-secondary is-size-5">
                    { choice.description()}
                </div>
            </div>
            <footer class="card-footer">
                <a href="#" class="card-footer-item">{ "Choose" }</a>
            </footer>
            </div>
            </a>
            </div>
        }
    }
    fn render_section(&self, section: &str) -> Html {
        html! {
            <p style="margin-bottom:2em">{ section.clone() }</p>
        }
    }
    fn render_debug_output(&self) -> Html {
        if self.state.debug {
            html! {
               <p>{ "State: "} { format!("{:?}", self.engine.state) }</p>
            }
        } else {
            html! {
                <p></p>
            }
        }
    }
}
