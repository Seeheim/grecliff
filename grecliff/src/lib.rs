#![recursion_limit = "256"]

pub mod app;

use core::story::io::StoryBuilder;
use core::story::Story;

pub fn build_story(story_string: &str, langs: &[(&str, &str)]) -> Result<Story, ()> {
    let mut builder = StoryBuilder::new(story_string).expect("Invalid story structure.");
    for (lang, content) in langs {
        builder
            .add_bundle((*lang).to_string(), (*content).to_string())
            .unwrap_or_else(|_| panic!("Invalid fluent bundle for {}", lang))
    }
    let story = builder.finish().unwrap();
    Ok(story)
}

pub fn init_from_str(story_string: &str, langs: &[(&str, &str)], version: String) {
    // web_logger::init();
    // trace!("Initializing yew...");
    yew::initialize();
    // trace!("Loading story...");

    let mut story = build_story(story_string, langs).unwrap();
    story.set_version(version);
    let properties = app::StoryAppProperties { story };
    // trace!("Creating an application instance...");
    let app: yew::App<app::StoryApp> = yew::App::new();
    // trace!("Mount the App to the body of the page...");

    app.mount_to_body_with_props(properties);
    // trace!("Run");
    yew::run_loop();
}
