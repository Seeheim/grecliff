extern crate clap;
use clap::{App, Arg};
use core::story::io::read_structure;

fn main() {
    let matches = App::new("Create fluent template")
        .version("0.1")
        .author("Seeheim <seeheim@protonmail.com>")
        .about("Creates an empty fluent file from a story skeleton file.")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .get_matches();

    // Calling .unwrap() is safe here because "INPUT" is required (if "INPUT" wasn't
    // required we could have used an 'if let' to conditionally get the value)
    let input_file = matches.value_of("INPUT").unwrap();
    let structure =
        read_structure(input_file).expect("Could not create story structure from input file");
    core::story::io::create_fluent_template(&structure, "out.txt")
        .expect("Failed to create template.");
}
