#![recursion_limit = "256"]
static STORY_SKELETON: &str = include_str!("../../assets/story.json");
static CONTENT_BUNDLE: &str = include_str!("../../assets/content/en-US.txt");

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn get_version_string() -> String {
    VERSION.to_string()
}


fn main() {
    grecliff::init_from_str(&STORY_SKELETON, &[("en-US", CONTENT_BUNDLE)], get_version_string())
}
